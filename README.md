# GraphQL interface to Tezos node data

## Summary

This GraphQL schema specifies an API interface for accessing data stored on a Tezos node. It aims to provide a partial replacement to the read-only information exposed via the current RPC API, while improving on usability and future compatibility.

## Motivation

The current RPC interface for Tezos node has some shortcomings for consumers, that would prefer to have the ability to filter/query by various operation types (`transactions`, `delegations`, ...) or ids (`operation_hash`).

For more details please see [Exposing Tezos Data via GraphQL](https://forum.tezosagora.org/t/exposing-tezos-data-via-graphql/1820) on Tezos Agora.

## Custom scalars
Schema is using custom scalars specified in [TZIP-14](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-14/tzip-14.md).

## Implementations

 - [TaaS-GraphQL @ GitHub](https://github.com/agile-ventures/TaaS-GraphQL)
 - [TezosLive.io](https://www.tezoslive.io)
